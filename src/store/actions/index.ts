import { ActionCreator } from 'redux';
import { IPost, IAction } from 'types';

export const fetchPosts: ActionCreator<IAction> = (): IAction => ({
  type: '@posts/fetch'
});

export const fetchPostsSuccess: ActionCreator<IAction> = (data: IPost[]): IAction => ({
  type: '@posts/fetch_success',
  payload: {
    data
  }
});

export const fetchPostsError: ActionCreator<IAction> = (): IAction => ({
  type: '@posts/fetch_error'
});

export const fetchSelectedPost: ActionCreator<IAction> = (): IAction => ({
  type: '@selectedPost/fetch'
});

export const fetchSelectedPostSuccess: ActionCreator<IAction> = (data: IPost): IAction => ({
  type: '@selectedPost/fetch_success',
  payload: {
    data
  }
});

export const fetchSelectedPostError: ActionCreator<IAction> = (): IAction => ({
  type: '@selectedPost/fetch_error'
});

export const assignSelectedPostSuccess: ActionCreator<IAction> = (data: IPost): IAction => ({
  type: '@selectedPost/assign',
  payload: {
    data
  }
});

export const goToDetailsRoute: ActionCreator<IAction> = (id: number, post?: IPost): IAction => ({
  type: 'DETAIL',
  payload: {
    id
  },
  meta: {
    post
  }
});

export const goToHomeRoute: ActionCreator<IAction> = (): IAction => ({
  type: 'HOME'
});
