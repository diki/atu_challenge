import { Reducer } from 'redux';
import { ISelectedPostState } from 'types';

export const initialState: ISelectedPostState = {
  data: undefined,
  fetching: false,
  success: false,
  error: false
};

const selectedPostReducer: Reducer<ISelectedPostState> = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    case '@selectedPost/fetch':
      return {
        ...state,
        fetching: true,
        success: false,
        error: false,
        data: undefined
      };
    case '@selectedPost/fetch_success':
    case '@selectedPost/assign':
      return {
        ...state,
        fetching: false,
        success: true,
        error: false,
        ...payload
      };
    case '@selectedPost/fetch_error':
    case '@selectedPost/reset':
      return {
        ...state,
        error: true,
        data: undefined
      };
    default:
      return state;
  }
};

export default selectedPostReducer;
