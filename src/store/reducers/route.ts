import { connectRoutes } from 'redux-first-router';

export const routesMap = {
  HOME: '/',
  DETAIL: '/post/:id'
};

const { reducer } = connectRoutes(routesMap);

export default reducer;
