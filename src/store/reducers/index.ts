import { combineReducers } from 'redux';
import { IStore, IAction } from 'types';
import posts from './posts';
import selectedPost from './selectedPost';
import location from './route';

const rootReducer = combineReducers<IStore, IAction>({
  posts, // for all posts
  selectedPost, // for single post
  location
});

export default rootReducer;
