import { Reducer } from 'redux';
import { IPostsState } from 'types';

export const initialState: IPostsState = {
  data: [],
  fetching: false,
  success: false,
  error: false
};

const postsReducer: Reducer<IPostsState> = (state = initialState, { type, payload }) => {
  switch (type) {
    case '@posts/fetch':
      return {
        ...state,
        fetching: true,
        success: false,
        error: false,
        data: []
      };
    case '@posts/fetch_success':
      return {
        ...state,
        fetching: false,
        success: true,
        error: false,
        ...payload
      };
    case '@posts/fetch_error':
      return {
        ...state,
        error: true,
        data: []
      };
    default:
      return state;
  }
};

export default postsReducer;
