import { ofType, Epic } from 'redux-observable';
import { switchMap, map, catchError, take } from 'rxjs/operators';
import { fromFetch } from 'rxjs/fetch';
import { of, concat, from } from 'rxjs';

import { IStore, IAction } from 'types';

const postsEpic: Epic<IAction, IAction, IStore> = (
  action$,
  state$,
  { getJSON }: { getJSON: typeof fromFetch }
) => {
  return action$.pipe(
    ofType('HOME'), // when { type: 'HOME' } action is dispatched
    take(1),
    switchMap(() => {
      return concat(
        of({
          type: '@posts/fetch'
        }),
        from(getJSON('https://jsonplaceholder.typicode.com/posts'))
          .pipe(
            switchMap(response => response.json()),
            catchError(_ => {
              return of({
                type: '@post/fetch_error'
              });
            })
          )
          .pipe(
            map(data => ({
              type: '@posts/fetch_success',
              payload: {
                data
              }
            }))
          )
      );
    })
  );
};

export default postsEpic;
