import { ActionsObservable } from 'redux-observable';
import selectedPostEpic from './index';

describe('Posts fetch epic', () => {
  it('On successful fetch should dispatch @selectedPost/fetch_success', async () => {
    const action$ = ActionsObservable.of({
      type: 'DETAIL',
      payload: {},
      meta: {}
    });
    const state$ = null;

    const getJSON = jest.fn();
    getJSON.mockReturnValue(
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve({ response: { total: 5 } })
      })
    );
    const output$ = selectedPostEpic(action$, state$, { getJSON });

    const result = await output$.toPromise();
    expect(result).toEqual({
      payload: { data: { response: { total: 5 } } },
      type: '@selectedPost/fetch_success'
    });
  });
});
