import { ofType, Epic } from 'redux-observable';
import { switchMap, map, catchError } from 'rxjs/operators';
import { fromFetch } from 'rxjs/fetch';
import { of, concat, from, iif } from 'rxjs';

import { IStore, IAction } from 'types';

const selectedPostEpic: Epic<IAction, IAction, IStore> = (
  action$,
  state$,
  { getJSON }: { getJSON: typeof fromFetch }
) => {
  return action$.pipe(
    ofType('DETAIL'), // when { type: 'DETAIL' } action is dispatched
    switchMap(({ meta, payload }) =>
      iif(
        () => meta.post !== undefined, // if there is meta this means it comes from click on list
        of({
          // dispatch assign event, it's coming from click
          type: '@selectedPost/assign',
          payload: {
            data: meta.post
          }
        }),
        // else it means it's opened directly from the browser
        // fetch the post
        concat(
          of({
            type: '@selectedPost/fetch'
          }),
          from(getJSON(`https://jsonplaceholder.typicode.com/posts/${payload.id}`))
            .pipe(
              switchMap(response => response.json()),
              catchError(_ => {
                return of({
                  type: '@selectedPost/fetch_error'
                });
              })
            )
            .pipe(
              map(data => ({
                type: '@selectedPost/fetch_success',
                payload: {
                  data
                }
              }))
            )
        )
      )
    )
  );
};

export default selectedPostEpic;
