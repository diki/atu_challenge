import { combineEpics } from 'redux-observable';
import postsEpic from './postsEpic';
import selectedPostEEpic from './selectedPostEpic';

export default combineEpics(postsEpic, selectedPostEEpic);
