import { ActionsObservable } from 'redux-observable';
import postsEpic from './index';

describe('Posts fetch epic', () => {
  it('On successful fetch should dispatch @posts/fetch_success', async () => {
    const action$ = ActionsObservable.of({
      type: 'HOME'
    });
    const state$ = null;

    const getJSON = jest.fn();
    getJSON.mockReturnValue(
      Promise.resolve({
        ok: true,
        json: () => Promise.resolve({ response: { total: 5 } })
      })
    );
    const output$ = postsEpic(action$, state$, { getJSON });

    const result = await output$.toPromise();
    expect(result).toEqual({
      payload: { data: { response: { total: 5 } } },
      type: '@posts/fetch_success'
    });
  });
});
