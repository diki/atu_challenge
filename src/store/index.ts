import { createStore, compose, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { fromFetch } from 'rxjs/fetch';
import { connectRoutes } from 'redux-first-router';

import { IAction, IStore } from 'types';
import rootReducer from './reducers';
import { routesMap } from './reducers/route';
import rootEpic from './epics';

const epicMiddleware = createEpicMiddleware<IAction, IAction, IStore>({
  dependencies: { getJSON: fromFetch } // necessary for testing
});

const router = connectRoutes(routesMap, {
  initialDispatch: false
});
const { middleware, enhancer } = router;

const store = createStore<IStore, IAction, any, any>(
  rootReducer,
  {},
  compose(
    enhancer,
    applyMiddleware(middleware, epicMiddleware),
    // redux-dev-tools extension
    // tslint:disable: whitespace
    (<any>window).devToolsExtension
      ? (<any>window).devToolsExtension()
      : // tslint:disable-next-line: only-arrow-functions
        function(f: any) {
          return f;
        }
  )
);

epicMiddleware.run(rootEpic);

// kick off the the first route dispatch
(router as any).initialDispatch();

export default store;
