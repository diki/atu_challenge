import { IPost } from '.';

export interface IStore {
  posts: IPostsState;
  selectedPost: ISelectedPostState;
  location: any; // reducer for routes
}

export interface IAction {
  type: string;
  payload?: any;
  meta?: any;
}

export interface IPostsState {
  data: IPost[];
  fetching: boolean;
  success: boolean;
  error: boolean;
}

export interface ISelectedPostState {
  data?: IPost;
  fetching: boolean;
  success: boolean;
  error: boolean;
}
