import * as actions from 'store/actions';

export interface IPost {
  userId: number;
  id: number;
  title: string;
  body: string;
}

export { IStore, IAction, IPostsState, ISelectedPostState } from './store';

export type Actions = {
  [fnName in keyof typeof actions]: typeof actions[fnName];
};
