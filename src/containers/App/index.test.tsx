import React from 'react';
import { shallow } from 'enzyme';
import { App } from './index';

const createShallowWrapper = props =>
  shallow(
    <App
      posts={{
        data: [
          {
            id: 1,
            userId: 2,
            title: 'title',
            body: 'body'
          }
        ],
        success: true,
        fetching: false,
        error: false
      }}
      actions={{
        goToDetailsRoute: jest.fn(),
        goToHomeRoute: jest.fn()
      }}
      {...props}
    />
  );
describe('Table component', () => {
  const mockFetch = jest.fn();

  it('should render list when location is HOME', () => {
    const wrapper = createShallowWrapper({ location: { type: 'HOME' } });
    expect(wrapper.find('PostList')).toBeDefined();
  });

  it('should render details when location is DETAIL', () => {
    const wrapper = createShallowWrapper({
      location: { type: 'HOME' },
      selectedPost: {
        data: [
          {
            id: 1,
            userId: 2,
            title: 'title',
            body: 'body'
          }
        ]
      }
    });
    expect(wrapper.find('PostDetails')).toBeDefined();
  });

  it('should render details when location is DETAIL', () => {
    const wrapper = createShallowWrapper({
      location: { type: 'HOME' },
      selectedPost: {
        data: [
          {
            id: 1,
            userId: 2,
            title: 'title',
            body: 'body'
          }
        ]
      }
    });
    expect(wrapper.find('PostDetails')).toBeDefined();
  });
});
