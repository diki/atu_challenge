import React from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';

import * as actions from 'store/actions';
import PostList from 'components/PostList';
import PostDetails from 'components/PostDetails';
import { Actions, IPostsState, IStore, ISelectedPostState } from 'types';

interface IDispatchProps {
  actions: {
    goToDetailsRoute: () => void;
    goToHomeRoute: () => void;
  };
}

interface IStateProps {
  posts: IPostsState;
  selectedPost: ISelectedPostState;
  location: any;
}

type PropsType = IStateProps & IDispatchProps;

export const App: React.SFC<PropsType> = props => {
  const { posts, actions, location, selectedPost } = props;

  let child = null;
  if (location.type === 'HOME') {
    child = (
      <div>
        <h1>Posts</h1>
        <PostList items={posts.data} onClickItem={actions.goToDetailsRoute} />
      </div>
    );
  }

  if (location.type === 'DETAIL' && selectedPost.data !== undefined) {
    child = (
      <div>
        <a className="link pointer blue" onClick={actions.goToHomeRoute}>
          {'< '} Go to list
        </a>
        <h1>Post details</h1>
        <PostDetails item={selectedPost.data} />
      </div>
    );
  }
  return <div className="pa4 mw8 center">{child}</div>;
};

const mapStateToProps = (state: IStore): IStateProps => ({
  posts: state.posts,
  selectedPost: state.selectedPost,
  location: state.location
});

const mapDispatchToProps = (dispatch: Dispatch): IDispatchProps => {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
};

export default connect<IStateProps, IDispatchProps, {}, IStateProps>(
  mapStateToProps,
  mapDispatchToProps
)(App);
