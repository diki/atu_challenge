import React from 'react';
import { shallow } from 'enzyme';
import List from './index';

describe('List component', () => {
  const mockOnClick = jest.fn();
  const wrapper = shallow(
    <List
      onClickItem={mockOnClick}
      items={[
        {
          id: 1,
          userId: 2,
          title: 'title',
          body: 'body'
        }
      ]}
    />
  );

  it('should have correct number of rows', () => {
    expect(wrapper.find('tbody tr')).toHaveLength(1);
  });

  it('should call onClickItem on click', () => {
    wrapper.find('tbody tr').simulate('click');
    expect(mockOnClick).toBeCalled();
  });
});
