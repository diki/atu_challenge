import * as React from 'react';
import { IPost } from 'types';

interface IPropsType {
  items: IPost[];
  onClickItem: (id: number, item: IPost) => void;
}

const PostList: React.SFC<IPropsType> = props => {
  const { items } = props;
  const rows = items.map((item: IPost) => (
    <tr key={`item_${item.id}`} onClick={() => props.onClickItem(item.id, item)}>
      <td className="pv3 pr3 bb b--black-20">{item.id}</td>
      <td className="pv3 pr3 bb b--black-20">{item.userId}</td>
      <td className="pv3 pr3 bb b--black-20">{item.title}</td>
      <td className="pv3 pr3 bb b--black-20">{item.body}</td>
    </tr>
  ));

  return (
    <table className="f6 w-100" cellSpacing="0">
      <thead>
        <tr>
          <th className="fw6 bb b--black-20 tl pb3 pr3 bg-white">Id</th>
          <th className="fw6 bb b--black-20 tl pb3 pr3 bg-white">User</th>
          <th className="fw6 bb b--black-20 tl pb3 pr3 bg-white">Title</th>
          <th className="fw6 bb b--black-20 tl pb3 pr3 bg-white">Body</th>
        </tr>
      </thead>
      <tbody className="lh-copy">{rows}</tbody>
    </table>
  );
};

export default PostList;
