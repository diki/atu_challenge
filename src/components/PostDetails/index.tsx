import React from 'react';
import { IPost } from 'types';

interface IPropsType {
  item: IPost;
}

const PostDetails: React.SFC<IPropsType> = props => {
  const { item } = props;
  return (
    <div>
      <div>Id: {item.id}</div>
      <div>User Id:{item.userId}</div>
      <div>Title: {item.title}</div>
      <div>Body: {item.body}</div>
    </div>
  );
};

export default PostDetails;
