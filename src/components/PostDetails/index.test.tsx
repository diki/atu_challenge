import React from 'react';
import { shallow } from 'enzyme';
import Details from './index';

describe('Details component', () => {
  const mockOnChange = jest.fn();
  const wrapper = shallow(
    <Details
      item={{
        id: 1,
        userId: 2,
        title: 'title',
        body: 'body'
      }}
    />
  );

  it('should have correct number of items', () => {
    expect(wrapper.find('div > div')).toHaveLength(4);
  });
});
