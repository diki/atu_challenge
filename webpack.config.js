const path = require('path');

module.exports = {
  entry: ['babel-polyfill', './src/entry.tsx'],
  devtool: 'cheap-module-source-map',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.json']
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      }
    ]
  },
  devServer: {
    compress: true,
    port: 8080,
    historyApiFallback: true,
    open: true,
    hot: true
  }
};
