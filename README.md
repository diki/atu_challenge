# Setup
- Run `yarn` or `npm install` to install dependencies
- Run `yarn start` or `npm start` to run the application. After running you should be able to see the app on http://localhost:8080
- Run `yarn build` to get a production build. This will create a folder named build including prod. bundles
- Run `yarn test` to run the tests

# Tools
- Typescript
- webpack 4 and Babel 7 is used to for bundling
- webpack-dev-server for development server
- Jest and Enzyme for testing
- Prettier and tslint for linting

# Notes
- All requirements are implemented. Home route '/' opens the post list and 'post/{id}' route opens the post detail page. You can opem post details page directly or through clicking post list.

- I used my own [very small boilerplate](https://bitbucket.org/diki/react-ts-starter) for building and bundling. I find create-react-app unnecessarily detailed (it makes sense they need to handle all users' cases) also AFAIK create-react-app uses typescript compiler for TS and then bundle the code with Webpack. This almost doubles build time comparing to mine because I skip TS compiling part and use babel-typescript-loader directly.

- You recommend react-router for routing but I use redux-first-router. Why? With react-router you cannot create route actions. This results that putting `componetDidMount` lifecycle to components for the initial fetch. Also I think component hiearchy becomes more cluttered with react-router. On the other hand redux-first-router handles route changes as regular redux actions. This way you can stick with your general app architecture, also you have much more control over route events and you can keep your views much clear.

- I did not use SASS for styling because firstly building SCSS increase the build times considerably. Second and more importantly SASS node module comes with C bindings, this might be problematic while running/building the project on different node versions. I used Tachyons for all styling (https://tachyons.io/). Tachyons is a tiny and a solid design system with very well designed class names.

- Application is built on top of redux and as you can realize all views are pure functions. I am also using redux-observable middleware. I am aware that redux-observable/RxJS might be a bit overkill for the scope of the project, just wanted to share my approach. I find RxJS is very declarative and also if you need refactoring, from the experience i can say it's the easiest. I could use also use thunk or saga middleware.

